package com.allstate.spring.project.AllstateSpringProject.repo;

import com.allstate.spring.project.AllstateSpringProject.entities.Claims;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClaimsRepo extends JpaRepository<Claims, Integer>{
}
