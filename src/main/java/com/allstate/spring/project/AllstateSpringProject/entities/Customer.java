package com.allstate.spring.project.AllstateSpringProject.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="customers")
public class Customer implements Serializable{

    @Id
    @Column(name="customer_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int customerId;

    @Column(name="first_name")
    private String firstName;

    @Column(name="last_name")
    private String lastName;

    @Column(name="city")
    private String city;

    @Column(name="zip")
    private int zip;

    @Column(name="start_date")
    private String startDate;

    public int getCustomerID(){
        return customerId;
    }

    public void setCustomerID(int CustomerID){
        this.customerId = CustomerID;
    }

    public String getFirstName(){
        return firstName;
    }

    public void setFirstName(String FirstName){
        this.firstName = FirstName;
    }

    public String getLastName(){
        return lastName;
    }

    public void setLastName(String LastName){
        this.lastName = LastName;
    }

    public String getCity(){
        return city;
    }

    public void setCity(String City){
        this.city = City;
    }

    public int getZip(){
        return zip;
    }

    public void setZip(int ZIP){
        this.zip = ZIP;
    }

    public String getStartDate(){
        return startDate;
    }

    public void setStartDate(String StartDate){
        this.startDate = StartDate;
    }
}
