package com.allstate.spring.project.AllstateSpringProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AllstateSpringProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(AllstateSpringProjectApplication.class, args);
	}

}
