package com.allstate.spring.project.AllstateSpringProject.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="claims")
public class Claims implements Serializable {

    @Id
//    @JoinColumn(name="claims_id", referencedColumnName = "customer_id", nullable = false)
//    @ManyToOne
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int claimsId;


    @Column(name="claim_number")
    private int claimNumber;

    @Column(name="claim_type")
    private String claimType;

    @Column(name="claim_amount")
    private int claimAmount;

    @Column(name="status")
    private String status;

    @Column(name="date")
    private String date;






    public int getClaimsId(){
        return claimsId;
    }

    public void setClaimsId(int ClaimsID){
        this.claimsId = ClaimsID;
    }

    public int getClaimNumber(){
        return claimNumber;
    }

    public void setClaimNumber(int ClaimNumber){
        this.claimNumber = ClaimNumber;
    }

    public String getClaimType(){
        return claimType;
    }

    public void setClaimType(String ClaimType){
        this.claimType = ClaimType;
    }

    public String getStatus(){
        return status;
    }

    public void setStatus(String Status){
        this.status = Status;
    }

    public String getDate(){
        return date;
    }

    public void setDate(String Date){
        this.date = Date;
    }




}
