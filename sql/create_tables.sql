CREATE TABLE `conygre`.`customers` (
                                       `customer_id` INT NOT NULL AUTO_INCREMENT,
                                       `first_name` VARCHAR(45) NOT NULL,
                                       `last_name` VARCHAR(45) NOT NULL,
                                       `city` VARCHAR(45) NOT NULL,
                                       `zip` INT NOT NULL,
                                       `start_date` VARCHAR(45) NOT NULL,
                                       PRIMARY KEY (`customer_id`));

CREATE TABLE `conygre`.`claims` (
                                    `claims_id` INT NOT NULL AUTO_INCREMENT,
                                    `claim_number` INT NOT NULL,
                                    `claim_type` VARCHAR(45) NOT NULL,
                                    `claim_amount` INT NOT NULL,
                                    `status` VARCHAR(45) NOT NULL,
                                    `date` VARCHAR(45) NOT NULL,
                                    `customer_id` INT NULL,
                                    PRIMARY KEY (`claims_id`),
                                    INDEX `customer_id_idx` (`customer_id` ASC) VISIBLE,
                                    CONSTRAINT `customer_id`
                                        FOREIGN KEY (`customer_id`)
                                            REFERENCES `conygre`.`customers` (`customer_id`)
                                            ON DELETE SET NULL
                                            ON UPDATE NO ACTION);



INSERT INTO conygre.customers (first_name, last_name, city, zip, start_date) VALUES('Dan','Stewart','Charlotte',28105,'01/01/2021');
INSERT INTO conygre.claims (claim_number, claim_type, claim_amount, `status`, `date`, customer_id) VALUES(463,'Auto',1000,'Pending','1/1/2021', 1);


/*
 FRANK - JUST IN CASE!

 CREATE TABLE `claims` (
  `ClaimID` int NOT NULL AUTO_INCREMENT,
  `CustomerID` int DEFAULT NULL,
  `ClaimNumber` int NOT NULL,
  `Date` varchar(45) NOT NULL,
  `ClaimAmount` int NOT NULL,
  `Status` varchar(45) NOT NULL,
  PRIMARY KEY (`ClaimID`),
  KEY `CustomerID_idx` (`CustomerID`),
  CONSTRAINT `CustomerID` FOREIGN KEY (`CustomerID`) REFERENCES `customers` (`CustomerID`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

 */